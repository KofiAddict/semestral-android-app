package cz.cuni.mff.semestralvolumeone;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import cz.cuni.mff.semestralvolumeone.ImageHandling.MyImageGetter;

/**
 * Activity for viewing news detail.
 */
public class NewsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        String content = bundle.getString("content");
        setTitle(bundle.getString("title"));

        TextView textView = findViewById(R.id.news_content);
        MyImageGetter p = new MyImageGetter(textView, this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT, p ,null));
        } else {
            textView.setText(Html.fromHtml(content, p ,null));
        }
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
