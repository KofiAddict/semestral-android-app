package cz.cuni.mff.semestralvolumeone.ImageHandling;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.widget.TextView;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.javanet.NetHttpTransport;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

public class MyImageGetter implements Html.ImageGetter {
    Context c;
    TextView container;

    public MyImageGetter(TextView t, Context c) {
        this.c = c;
        this.container = t;
    }

    public Drawable getDrawable(String source) {
        DrawableWrapper drawableWrapper = new DrawableWrapper();
        ImageGetterAsyncTask asyncTask = new ImageGetterAsyncTask(drawableWrapper, container);

        asyncTask.execute(source);
        return drawableWrapper;
    }

    public class ImageGetterAsyncTask extends AsyncTask<String, Void, Drawable> {
        DrawableWrapper drawableWrapper;
        TextView container;


        public ImageGetterAsyncTask(DrawableWrapper d, TextView container) {
            this.drawableWrapper = d;
            this.container = container;
        }

        @Override
        protected Drawable doInBackground(String... params) {
            String source = params[0];
            return fetchDrawable(source);
        }

        @Override
        protected void onPostExecute(Drawable result) {
            drawableWrapper.setBounds(0, 0, result.getIntrinsicWidth(), result.getIntrinsicHeight());
            drawableWrapper.drawable = result;
            MyImageGetter.this.container.invalidate();
            container.setText(container.getText());
        }

        public Drawable fetchDrawable(String urlString) {
            try {
                InputStream is = fetch(urlString);
                Drawable drawable = Drawable.createFromStream(is, "src");
                drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                return drawable;
            } catch (Exception e) {
                return null;
            }
        }

        private InputStream fetch(String urlString) throws MalformedURLException, IOException {
            HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory();
            HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(urlString));
            HttpResponse response = request.execute();
            return response.getContent();
        }
    }
}