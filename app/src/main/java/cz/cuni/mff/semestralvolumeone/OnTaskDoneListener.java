package cz.cuni.mff.semestralvolumeone;

/**
 * This is an interface for callbacks of tasks.
 */
public interface OnTaskDoneListener {
    void onTaskDone(String responseData);
}
