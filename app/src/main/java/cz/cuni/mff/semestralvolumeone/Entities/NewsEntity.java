package cz.cuni.mff.semestralvolumeone.Entities;

import java.io.Serializable;

public final class NewsEntity implements Serializable {
    public AppNews appnews;

    public class AppNews implements Serializable {
        int appid;
        public News[] newsitems;
        int count;

        public class News implements Serializable {
            long gid;
            public String title;
            String url;
            String author;
            public String contents;
            short date;
        }
    }
}
