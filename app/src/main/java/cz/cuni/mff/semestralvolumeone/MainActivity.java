package cz.cuni.mff.semestralvolumeone;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import cz.cuni.mff.semestralvolumeone.Adapters.AppListAdapter;
import cz.cuni.mff.semestralvolumeone.Entities.App;
import cz.cuni.mff.semestralvolumeone.Entities.AppEntity;
import cz.cuni.mff.semestralvolumeone.Tasks.RetrieveAppsJsonTask;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase sqLiteDatabase;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SQLiteDatabase sqLiteDatabase = openOrCreateDatabase("main-database", MODE_PRIVATE, null);
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='main';", null);
        if (cursor.getCount() == 0) {
            sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS main(appid NUMBER, appname VARCHAR, appnamelowercase VARCHAR);");
            sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS index_names_lowercase ON main(appnamelowercase)");
            sqLiteDatabase.execSQL("DELETE FROM main");

            new RetrieveAppsJsonTask(responseData -> {
                Gson gson = new Gson();
                AppEntity appEntity = gson.fromJson(responseData, AppEntity.class);
                StringBuilder stringBuilder = new StringBuilder(10000000);
                for (App entity : appEntity.applist.apps){
                    stringBuilder.append("(");
                    stringBuilder.append(entity.appid);
                    stringBuilder.append(",\"");
                    stringBuilder.append(entity.name.replaceAll("\"", "\\'"));
                    stringBuilder.append("\",\"");
                    stringBuilder.append(entity.name.toLowerCase().replaceAll("\"", "\\'"));
                    stringBuilder.append("\"),");
                }
                String insert = stringBuilder.toString();
                insert = insert.substring(0, insert.length() - 1);
                insert = "INSERT INTO main (appid,appname,appnamelowercase) VALUES" + insert + ";";
                sqLiteDatabase.execSQL(insert);
            }).execute();
        }
        cursor.close();
        this.sqLiteDatabase = sqLiteDatabase;

        final RecyclerView recyclerView = findViewById(R.id.main_content_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new AppListAdapter(getFromSqlite("")));


        final EditText text = findViewById(R.id.search_bar_input);
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void afterTextChanged(Editable s) {
                recyclerView.setAdapter(new AppListAdapter(getFromSqlite(s.toString())));
            }
        });


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS main(appid NUMBER, appname VARCHAR, appnamelowercase VARCHAR);");
            sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS index_names_lowercase ON main(appnamelowercase)");
            sqLiteDatabase.execSQL("DELETE FROM main");
            new RetrieveAppsJsonTask(responseData -> {
                Gson gson = new Gson();
                AppEntity appEntity = gson.fromJson(responseData, AppEntity.class);
                StringBuilder stringBuilder = new StringBuilder(10000000);
                for (App entity : appEntity.applist.apps){
                    stringBuilder.append("(");
                    stringBuilder.append(entity.appid);
                    stringBuilder.append(",\"");
                    stringBuilder.append(entity.name.replaceAll("\"", "\\'"));
                    stringBuilder.append("\",\"");
                    stringBuilder.append(entity.name.toLowerCase().replaceAll("\"", "\\'"));
                    stringBuilder.append("\"),");
                }
                String insert = stringBuilder.toString();
                insert = insert.substring(0, insert.length() - 1);
                insert = "INSERT INTO main (appid,appname,appnamelowercase) VALUES" + insert + ";";
                sqLiteDatabase.execSQL(insert);
            }).execute();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    ArrayList<App> getFromSqlite(String query) {
        String sql = "SELECT * FROM main WHERE instr(appnamelowercase, \"" + query.toLowerCase().replaceAll("\"", "\\'") + "\") > 0 LIMIT 200";
        if (query.isEmpty()) {
            sql = "SELECT * FROM main LIMIT 200";
        }
        Cursor resultSet = sqLiteDatabase.rawQuery(sql, null);
        ArrayList<App> apps = new ArrayList<>();
        int count = resultSet.getCount();

        resultSet.moveToFirst();
        for (int i = 0; i < count; i++) {
            App newApp = new App();
            newApp.appid = resultSet.getInt(resultSet.getColumnIndex("appid"));
            newApp.name = resultSet.getString(resultSet.getColumnIndex("appname"));
            apps.add(newApp);

            resultSet.moveToNext();
        }

        resultSet.close();
        return apps;
    }
}

